function ibm_seizures_generate_new_categories_data

file_dir = 'pp2_mat';
seiz_types = {'FNSZ'; 'GNSZ'; 'SPSZ'; 'CPSZ'; 'ABSZ'; 'TNSZ'; 'TCSZ'};
n_training_types = 4; %4 for training, 3 for test?
n_per_combo = 10000; %10000 would give us 120000 "training" total
feat_dim = 900;
outname_format_train = '/Volumes/FilesOther/__transfer_TO_work_temp/newcats/ptc_ibmseiz_newcat_train_%d%d%d%d_test_%d%d%d_traindata.mat';
outname_format_test  = '/Volumes/FilesOther/__transfer_TO_work_temp/newcats/ptc_ibmseiz_newcat_train_%d%d%d%d_test_%d%d%d_testdata_seiztype%d.mat';


rng('shuffle');

matfiles = dir( fullfile(file_dir,'*.mat') );
matfiles = {matfiles.name};
n_matfiles = numel(matfiles);

n_seiz_types = numel(seiz_types);
train_sets = combnk(1:n_seiz_types, n_training_types);
type_file_inds  = cell(n_seiz_types,1);
type_file_ns = nan(n_seiz_types,1);
tmp_file_inds = nan(n_matfiles,1);

for i = 1:n_matfiles
    tmp = load(fullfile(file_dir,matfiles{i}));
    tmp_type = find(strcmp(seiz_types,tmp.seiz_type));
    if ~isempty(tmp_type)
        tmp_file_inds(i) = tmp_type;
    end
end

for i = 1:n_seiz_types
    type_file_inds{i} = find(tmp_file_inds==i);
    type_file_ns(i) = numel(type_file_inds{i});
end

for i = 1:size(train_sets,1)
    fprintf('Set %d of %d\n',i,size(train_sets,1));
    this_train_set = train_sets(i,:);
    this_test_set = setdiff(1:n_seiz_types,this_train_set);
    
    %new for this: load "train" data
    n_train_seiz_total = sum(type_file_ns(this_train_set));
    train_seiz_data_cell = cell(n_train_seiz_total,1);
    train_seiz_label_cell = cell(n_train_seiz_total,1);
    train_seiz_filetag_cell = cell(n_train_seiz_total,1);
    train_seiz_ctr = 1;
    for j = this_train_set
        tmp_file_inds = type_file_inds{j}';
        for k = tmp_file_inds
            tmp = load(fullfile(file_dir,matfiles{k}));
            train_seiz_data_cell{train_seiz_ctr} = tmp.seiz_data;
            train_seiz_label_cell{train_seiz_ctr} = ones(size(tmp.seiz_data,1),1) * j;
            train_seiz_filetag_cell{train_seiz_ctr} = ones(size(tmp.seiz_data,1),1) * k;
            train_seiz_ctr = train_seiz_ctr + 1;
        end
    end
    train_seiz_labels = vertcat(train_seiz_label_cell{:}); clear train_seiz_label_cell;
    train_seiz_filetags = vertcat(train_seiz_filetag_cell{:}); clear train_seiz_filetag_cell;
    train_seiz_data = vertcat(train_seiz_data_cell{:}); clear train_seiz_data_cell;
    
    train_diff_combos = combnk(this_train_set,2);
    train_diff_combos = [train_diff_combos; fliplr(train_diff_combos)]; %#ok<AGROW>
    n_train_diff_combos = size(train_diff_combos,1);
    n_diffs_total = n_train_diff_combos * n_per_combo;
    
    diff_dat = zeros(n_train_diff_combos,n_per_combo,2,feat_dim);
    diff_st1 = zeros(n_train_diff_combos,n_per_combo);
    diff_st2 = zeros(n_train_diff_combos,n_per_combo);
    diff_ft1 = zeros(n_train_diff_combos,n_per_combo); %ft = file tag
    diff_ft2 = zeros(n_train_diff_combos,n_per_combo);
    for j = 1:n_train_diff_combos
        fprintf('- diff combo %d of %d\n',j,n_train_diff_combos);
        st1_inds = find(train_seiz_labels==train_diff_combos(j,1)); %st = seizure type
        st2_inds = find(train_seiz_labels==train_diff_combos(j,2));
        
%         [tmp1, tmp2] = meshgrid(uint32(1:numel(st1_inds)),uint32(1:numel(st2_inds)));
%         possible_diff_inds = [tmp1(:) tmp2(:)]; %a little thorny b/c these are really indices of indices
%         n_possible_diff_inds = size(possible_diff_inds,1);
%         possible_diff_inds_rand_inds = randperm(n_possible_diff_inds, n_per_combo);
%         
%         diff_inds_to_use = possible_diff_inds(possible_diff_inds_rand_inds,:);
        
        %too much RAM the old way, now trying this
        possible_diff_inds1 = randi(numel(st1_inds),[n_per_combo*2, 1]); %double for safety
        possible_diff_inds2 = randi(numel(st2_inds),[n_per_combo*2, 1]); %double for safety
        possible_diff_inds  = [possible_diff_inds1 possible_diff_inds2];
        
        possible_diff_inds = unique(possible_diff_inds,'rows','stable');
        diff_inds_to_use = possible_diff_inds(1:n_per_combo,:);
        
        st1_inds_to_use = st1_inds(diff_inds_to_use(:,1));
        st2_inds_to_use = st2_inds(diff_inds_to_use(:,2));
        diff_data1 = train_seiz_data(st1_inds_to_use,:);
        diff_data2 = train_seiz_data(st2_inds_to_use,:);
        
        diff_dat(j,:,1,:) = diff_data1;
        diff_dat(j,:,2,:) = diff_data2;
        
        diff_st1(j,:) = train_diff_combos(j,1);
        diff_st2(j,:) = train_diff_combos(j,2);
        diff_ft1(j,:) = train_seiz_filetags(st1_inds_to_use);
        diff_ft2(j,:) = train_seiz_filetags(st2_inds_to_use);
    end
    clear possible_diff_inds;
    clear diff_data1;
    clear diff_data2;
    clear tmp1;
    clear tmp2;
    
    n_sames_per_seiztype = n_diffs_total / n_training_types;
    same_dat = zeros(n_training_types,n_sames_per_seiztype,2,feat_dim);
    same_st1 = zeros(n_training_types,n_sames_per_seiztype);
    same_st2 = zeros(n_training_types,n_sames_per_seiztype);
    same_ft1 = zeros(n_training_types,n_sames_per_seiztype);
    same_ft2 = zeros(n_training_types,n_sames_per_seiztype);
    for j = 1:n_training_types
        fprintf('- same seiztype %d of %d\n',j,n_training_types);
        this_same_seiztype = this_train_set(j);
        same_seiztype_inds = find(train_seiz_labels==this_same_seiztype);
                
        %too much RAM the old way, now trying this
        possible_same_inds = randi(numel(same_seiztype_inds),[n_sames_per_seiztype*2, 2]); %double for safety
        possible_same_inds(possible_same_inds(:,1) == possible_same_inds(:,2), :) = []; %get rid of duplicates
        
        possible_same_inds = unique(possible_same_inds,'rows','stable');
        same_inds_to_use = possible_same_inds(1:n_sames_per_seiztype,:);
        
        st1_inds_to_use = same_seiztype_inds(same_inds_to_use(:,1));
        st2_inds_to_use = same_seiztype_inds(same_inds_to_use(:,2));
        same_data1 = train_seiz_data(st1_inds_to_use,:);
        same_data2 = train_seiz_data(st2_inds_to_use,:);
        
        same_dat(j,:,1,:) = same_data1;
        same_dat(j,:,2,:) = same_data2;
        
        same_st1(j,:) = this_same_seiztype;
        same_st2(j,:) = this_same_seiztype;
        same_ft1(j,:) = train_seiz_filetags(st1_inds_to_use);
        same_ft2(j,:) = train_seiz_filetags(st2_inds_to_use);
    end
    clear same_data1;
    clear same_data2;
    
    %write out "training" data/metadata
    clear train_seiz_labels train_seiz_filetags train_seiz_data;
    diff_dat = reshape(diff_dat,[n_train_diff_combos*n_per_combo,2,feat_dim]);
    same_dat = reshape(same_dat,[n_training_types*n_sames_per_seiztype,2,feat_dim]);
    training_seiztypes_data = [same_dat; diff_dat];
    training_seiztypes_samediff_labels = zeros(size(training_seiztypes_data,1),1);
    training_seiztypes_samediff_labels(1:size(same_dat,1)) = 1; %same
    training_seiztypes_samediff_labels(training_seiztypes_samediff_labels==0) = 2; %diff
    clear same_dat; clear diff_dat;
    
    diff_st1 = reshape(diff_st1, [n_train_diff_combos*n_per_combo,1] );
    diff_st2 = reshape(diff_st2, [n_train_diff_combos*n_per_combo,1] );
    same_st1 = reshape(same_st1, [n_training_types*n_sames_per_seiztype,1] );
    same_st2 = reshape(same_st2, [n_training_types*n_sames_per_seiztype,1] );
    
    training_seiztype1_tags = [same_st1; diff_st1];
    training_seiztype2_tags = [same_st2; diff_st2];
    
    diff_ft1 = reshape(diff_ft1, [n_train_diff_combos*n_per_combo,1] );
    diff_ft2 = reshape(diff_ft2, [n_train_diff_combos*n_per_combo,1] );
    same_ft1 = reshape(same_ft1, [n_training_types*n_sames_per_seiztype,1] );
    same_ft2 = reshape(same_ft2, [n_training_types*n_sames_per_seiztype,1] );
    
    training_file1_tags = [same_ft1; diff_ft1];
    training_file2_tags = [same_ft2; diff_ft2];
	
    output_types_tmp = num2cell([sort(this_train_set) sort(this_test_set)]);
    output_fname = sprintf(outname_format_train,output_types_tmp{:});
%     save(output_fname, 'training_seiztypes_data','training_seiztypes_samediff_labels',...
%                        'training_seiztype1_tags','training_seiztype2_tags',...
%                        'training_file1_tags','training_file2_tags', ...
%                        '-v7.3');
    
    %try this, adapted from savefast by Tim Holy
    dummy=0; %#ok<NASGU>
    save(output_fname,'dummy','-v7.3');
    fid = H5F.open(output_fname,'H5F_ACC_RDWR','H5P_DEFAULT');
    H5L.delete(fid,'dummy','H5P_DEFAULT');
    H5F.close(fid);
    % not totally sure why we have to permute/transpose but it seems OK...
    training_seiztypes_data = permute(training_seiztypes_data, [3 2 1]);
    training_seiztypes_samediff_labels = training_seiztypes_samediff_labels';
    training_seiztype1_tags = training_seiztype1_tags';
    training_seiztype2_tags = training_seiztype2_tags';
    training_file1_tags = training_file1_tags';
    training_file2_tags = training_file2_tags';
    h5create(output_fname, '/training_seiztypes_data', size(training_seiztypes_data), 'DataType', class(training_seiztypes_data));
    h5write(output_fname, '/training_seiztypes_data', training_seiztypes_data);
    h5create(output_fname, '/training_seiztypes_samediff_labels', size(training_seiztypes_samediff_labels), 'DataType', class(training_seiztypes_samediff_labels));
    h5write(output_fname, '/training_seiztypes_samediff_labels', training_seiztypes_samediff_labels);
    h5create(output_fname, '/training_seiztype1_tags', size(training_seiztype1_tags), 'DataType', class(training_seiztype1_tags));
    h5write(output_fname, '/training_seiztype1_tags', training_seiztype1_tags);
    h5create(output_fname, '/training_seiztype2_tags', size(training_seiztype2_tags), 'DataType', class(training_seiztype2_tags));
    h5write(output_fname, '/training_seiztype2_tags', training_seiztype2_tags);
    h5create(output_fname, '/training_file1_tags', size(training_file1_tags), 'DataType', class(training_file1_tags));
    h5write(output_fname, '/training_file1_tags', training_file1_tags);
    h5create(output_fname, '/training_file2_tags', size(training_file2_tags), 'DataType', class(training_file2_tags));
    h5write(output_fname, '/training_file2_tags', training_file2_tags);
	clear training_seiztypes_data;
    
    
    %new for this: clear "train" data, re-load all data
    % (seems weird but can't think of a better way to minimize memory use easily)
    n_all_seiz_total = sum(type_file_ns);
    all_seiz_data_cell = cell(n_all_seiz_total,1);
    all_seiz_label_cell = cell(n_all_seiz_total,1);
    all_seiz_filetag_cell = cell(n_all_seiz_total,1);
    all_seiz_ctr = 1;
    for j = 1:n_seiz_types
        tmp_file_inds = type_file_inds{j}';
        for k = tmp_file_inds
            tmp = load(fullfile(file_dir,matfiles{k}));
            all_seiz_data_cell{all_seiz_ctr} = tmp.seiz_data;
            all_seiz_label_cell{all_seiz_ctr} = ones(size(tmp.seiz_data,1),1) * j;
            all_seiz_filetag_cell{all_seiz_ctr} = ones(size(tmp.seiz_data,1),1) * k;
            all_seiz_ctr = all_seiz_ctr + 1;
        end
    end
    all_seiz_labels = vertcat(all_seiz_label_cell{:}); clear all_seiz_label_cell;
    all_seiz_filetags = vertcat(all_seiz_filetag_cell{:}); clear all_seiz_filetag_cell;
    all_seiz_data = vertcat(all_seiz_data_cell{:}); clear all_seiz_data_cell;
    
    test_examples_base_loginds = false(size(all_seiz_labels));
    for j = this_test_set
        test_examples_base_loginds = test_examples_base_loginds | (all_seiz_labels==j);
    end
    n_test_examples_base = sum(test_examples_base_loginds);
    
    for k = 1:n_seiz_types
        fprintf('- test seiztype %d of %d\n',k,n_seiz_types);
        test_dat = zeros(n_test_examples_base,2,2,feat_dim); % ??? x top/bottom x item1/item2 x features
        test_st1 = zeros(n_test_examples_base,2);
        test_st2 = zeros(n_test_examples_base,2);
        test_ft1 = zeros(n_test_examples_base,2);
        test_ft2 = zeros(n_test_examples_base,2);
        
        all_test_inds = find(test_examples_base_loginds);
        
        k_inds = find(all_seiz_labels == k);
        n_k_inds = numel(k_inds);
            
        for j = 1:n_test_examples_base
            this_j_ind = all_test_inds(j);
            other_ind1 = this_j_ind;
            while other_ind1 == this_j_ind
                other_ind1 = k_inds(randi(n_k_inds));
            end
            
            other_ind2 = this_j_ind;
            while other_ind2 == this_j_ind || other_ind1 == other_ind2
                other_ind2 = k_inds(randi(n_k_inds));
            end
            
            test_dat(j,1,1,:) = all_seiz_data(this_j_ind,:);
            test_dat(j,1,2,:) = all_seiz_data(other_ind1,:);
            
            test_dat(j,2,2,:) = all_seiz_data(this_j_ind,:);
            test_dat(j,2,1,:) = all_seiz_data(other_ind2,:);
            
            test_st1(j,1)     = all_seiz_labels(this_j_ind);
            test_st2(j,1)     = all_seiz_labels(other_ind1);
            
            test_st2(j,2)     = all_seiz_labels(this_j_ind);
            test_st1(j,2)     = all_seiz_labels(other_ind2);
            
            test_ft1(j,1)     = all_seiz_filetags(this_j_ind);
            test_ft2(j,1)     = all_seiz_filetags(other_ind1);
            
            test_ft2(j,2)     = all_seiz_filetags(this_j_ind);
            test_ft1(j,2)     = all_seiz_filetags(other_ind2);
        end
        
        
        %write out "test" data/metadata
        test_dat = reshape(test_dat,[n_test_examples_base*2,2,feat_dim]);
        test_st1 = reshape(test_st1,[n_test_examples_base*2,1]);
        test_st2 = reshape(test_st2,[n_test_examples_base*2,1]);
        test_ft1 = reshape(test_ft1,[n_test_examples_base*2,1]);
        test_ft2 = reshape(test_ft2,[n_test_examples_base*2,1]);
        
        output_types_tmp = num2cell([sort(this_train_set) sort(this_test_set) k]);
        output_fname = sprintf(outname_format_test,output_types_tmp{:});
%         save(output_fname, 'test_st1','test_st2','test_ft1','test_ft2',...
%                            'test_dat',...
%                            '-v7.3');
        
        %try this, adapted from savefast by Tim Holy
        dummy=0; %#ok<NASGU>
        save(output_fname,'dummy','-v7.3');
        fid = H5F.open(output_fname,'H5F_ACC_RDWR','H5P_DEFAULT');
        H5L.delete(fid,'dummy','H5P_DEFAULT');
        H5F.close(fid);
        % not totally sure why we have to permute/transpose but it seems OK...
        test_st1 = test_st1';
        test_st2 = test_st2';
        test_ft1 = test_ft1';
        test_ft2 = test_ft2';
        test_dat = permute(test_dat,[3 2 1]);
        h5create(output_fname, '/test_st1', size(test_st1), 'DataType', class(test_st1));
        h5write(output_fname, '/test_st1', test_st1);
        h5create(output_fname, '/test_st2', size(test_st2), 'DataType', class(test_st2));
        h5write(output_fname, '/test_st2', test_st2);
        h5create(output_fname, '/test_ft1', size(test_ft1), 'DataType', class(test_ft1));
        h5write(output_fname, '/test_ft1', test_ft1);
        h5create(output_fname, '/test_ft2', size(test_ft2), 'DataType', class(test_ft2));
        h5write(output_fname, '/test_ft2', test_ft2);
        h5create(output_fname, '/test_dat', size(test_dat), 'DataType', class(test_dat));
        h5write(output_fname, '/test_dat', test_dat);
    end
    
    clear all_seiz_labels all_seiz_filetags all_seiz_data;
    clear test_dat;
end

