function ibm_seizures_process_results

results_dir = 'results_for_processing';
result_filename_format = 'ptc_ibmseiz_newcat_testresults_train_1234_test_567_iter%.4d_seiztype%d.mat';
nits = 10;
n_seiz_types = 7;



for i = 1:nits
    itnum = i-1;
    fprintf('iteration %d of %d\n',i,nits);
    
    for j = 1:n_seiz_types
        fprintf('- seiztype %d of %d\n',j,n_seiz_types);
        
        fname = fullfile(results_dir,sprintf(result_filename_format,itnum,j));
        dat = load(fname);
        predsame = dat.preds(:,1);
        ft1 = dat.test_ft1;
        ft2 = dat.test_ft2;
        st1 = dat.test_st1;
        st2 = dat.test_st2;
        
        n_test_examples_base = numel(ft1) / (n_seiz_types*2);
        ft1 = reshape(ft1,[n_test_examples_base n_seiz_types 2]);
        ft2 = reshape(ft2,[n_test_examples_base n_seiz_types 2]);
        st1 = reshape(st1,[n_test_examples_base n_seiz_types 2]);
        st2 = reshape(st2,[n_test_examples_base n_seiz_types 2]);
        
        tmp = ft1;
        ft1(:,:,2) = ft2(:,:,2);
        ft2(:,:,2) = tmp(:,:,2);
        
        tmp = st1;
        st1(:,:,2) = st2(:,:,2);
        st2(:,:,2) = tmp(:,:,2);
        
        ft1 = reshape(ft1, n_test_examples_base*n_seiz_types*2, 1);
        ft2 = reshape(ft2, n_test_examples_base*n_seiz_types*2, 1); %#ok<NASGU>
        st1 = reshape(st1, n_test_examples_base*n_seiz_types*2, 1);
        st2 = reshape(st2, n_test_examples_base*n_seiz_types*2, 1); %#ok<NASGU>
        
        %after all that reshaping, now all ft/st 1's should be the item being tested
        
        if i==1 && j==1
            uniq_seizfiles = unique(ft1);
            n_uniq_files   = numel(uniq_seizfiles);
            preds_by_file  = nan(n_uniq_files,n_seiz_types,nits);
            accs_by_file   = nan(n_uniq_files,nits);
            
            uniq_seiztypes = unique(st1);
            n_uniq_sts     = numel(uniq_seiztypes);
            preds_by_st    = nan(n_uniq_sts, n_seiz_types, nits);
            
            file_seiztypes = nan(n_uniq_files,1);
            file_preds_by_seiztype = nan(n_uniq_sts, n_seiz_types, nits);
            file_accs_by_seiztype  = nan(n_uniq_sts, nits);
        end
        
        
        for k = 1:n_uniq_files
            this_seizfile = uniq_seizfiles(k);
            preds_by_file(k,j,i) = mean( predsame( ft1==this_seizfile ));
            
            if i==1 && j==1
                this_seizfile_ind = find( ft1==this_seizfile,1 );
                file_seiztypes(k) = st1(this_seizfile_ind);
            end
        end
        
        
        for k = 1:n_uniq_sts
            preds_by_st(k,j,i) = mean( predsame( st1==uniq_seiztypes(k) ));
            
            file_preds_by_seiztype(k,j,i) = mean( preds_by_file( file_seiztypes==uniq_seiztypes(k), j, i ), 1);
        end
        
    end
    
    for k = 1:n_uniq_files
        this_file_preds = squeeze(preds_by_file(k,:,i));
        accs_by_file(k,i) = find(this_file_preds==max(this_file_preds)) == file_seiztypes(k);
    end
    
    %pick up here, needs editing
    for k = 1:n_uniq_sts
        file_accs_by_seiztype(k,i) = mean( accs_by_file( file_seiztypes==uniq_seiztypes(k), i ), 1);
    end
    
end

keyboard;

