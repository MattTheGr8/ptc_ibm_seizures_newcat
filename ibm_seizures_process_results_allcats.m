function ibm_seizures_process_results_allcats

results_dir = 'results_allcats_try2';
result_filename_format = 'ptc_ibmseiz_allcats_testresults_set%.3d_iter%.4d.mat';
nits = 10;
nsets = 12;
n_seiz_types = 7;


for s = 1:nsets
    fprintf('Set %d of %d\n',s,nsets);
    for i = 1:nits
        itnum = i-1;
        fprintf('- iteration %d of %d\n',i,nits);
        
        fname = fullfile(results_dir,sprintf(result_filename_format,s,itnum));
        dat = load(fname);
        predsame = dat.preds(:,1);
        predsame_bin = predsame>.5;
%         ft1 = dat.test_ft1;
%         ft2 = dat.test_ft2;
        st1 = dat.test_st1;
        st2 = dat.test_st2;
        test_item_ft = dat.test_item_ft;
        test_item_st = dat.test_item_st;
        
%         other_item_ft = test_item_ft;
%         other_item_ft(test_item_ft~=ft1) = ft1(test_item_ft~=ft1);
%         other_item_ft(test_item_ft~=ft2) = ft1(test_item_ft~=ft2);
        
        other_item_st = test_item_st;
        other_item_st(test_item_st~=st1) = st1(test_item_st~=st1);
        other_item_st(test_item_st~=st2) = st1(test_item_st~=st2);
        
%         n_test_examples_base = numel(ft1) / (n_seiz_types*2);
%         ft1 = reshape(ft1,[n_test_examples_base n_seiz_types 2]);
%         ft2 = reshape(ft2,[n_test_examples_base n_seiz_types 2]);
%         st1 = reshape(st1,[n_test_examples_base n_seiz_types 2]);
%         st2 = reshape(st2,[n_test_examples_base n_seiz_types 2]);
% 
%         tmp = ft1;
%         ft1(:,:,2) = ft2(:,:,2);
%         ft2(:,:,2) = tmp(:,:,2);
% 
%         tmp = st1;
%         st1(:,:,2) = st2(:,:,2);
%         st2(:,:,2) = tmp(:,:,2);
% 
%         ft1 = reshape(ft1, n_test_examples_base*n_seiz_types*2, 1);
%         ft2 = reshape(ft2, n_test_examples_base*n_seiz_types*2, 1); %#ok<NASGU>
%         st1 = reshape(st1, n_test_examples_base*n_seiz_types*2, 1);
%         st2 = reshape(st2, n_test_examples_base*n_seiz_types*2, 1); %#ok<NASGU>
% 
%         %after all that reshaping, now all ft/st 1's should be the item being tested

        for j = 1:n_seiz_types
            fprintf('- seiztype %d of %d\n',j,n_seiz_types);

            if i==1 && j==1
                uniq_seizfiles = unique(test_item_ft);
                n_uniq_files   = numel(uniq_seizfiles);

                file_seiztypes = nan(n_uniq_files,1);
                
                if s==1 %kinda weird but easiest/laziest way to adapt from previous script
                    preds_by_st    = nan(n_seiz_types, n_seiz_types, nsets, nits);
                    preds_by_file  = nan(n_uniq_files,n_seiz_types,nsets,nits);
                    preds_by_file_binmean  = nan(n_uniq_files,n_seiz_types,nsets,nits);
                    accs_by_file   = nan(n_uniq_files,nsets,nits);
                    file_preds_by_seiztype = nan(n_seiz_types, n_seiz_types, nsets, nits);
                    file_accs_by_seiztype  = nan(n_seiz_types, nsets, nits);
                end
            end


            for k = 1:n_uniq_files
                this_seizfile = uniq_seizfiles(k);
                preds_by_file(k,j,s,i) = mean( predsame( test_item_ft==this_seizfile & other_item_st==j ));
                preds_by_file_binmean(k,j,s,i) = mean( predsame_bin( test_item_ft==this_seizfile & other_item_st==j ));

                if i==1 && j==1
                    this_seizfile_ind = find( test_item_ft==this_seizfile,1 );
                    file_seiztypes(k) = test_item_st(this_seizfile_ind);
                end
            end


            for k = 1:n_seiz_types
                preds_by_st(k,j,s,i) = mean( predsame( test_item_st==k & other_item_st==j ));

                file_preds_by_seiztype(k,j,s,i) = mean( preds_by_file( file_seiztypes==k, j, s, i ), 1); %actual type, pred type, set, iter
                file_preds_by_seiztype_bin(k,j,s,i) = mean( preds_by_file_binmean( file_seiztypes==k, j, s, i ), 1); %actual type, pred type, set, iter
            end

        end

        for k = 1:n_uniq_files
            this_file_preds = squeeze(preds_by_file(k,:,s,i));
            accs_by_file(k,s,i) = find(this_file_preds==max(this_file_preds)) == file_seiztypes(k);
        end

        for k = 1:n_seiz_types
            file_accs_by_seiztype(k,s,i) = mean( accs_by_file( file_seiztypes==k, s, i ), 1);
        end
        
        overall_same_acc_bysample(s,i) = mean(predsame_bin(st1==st2));
        overall_diff_acc_bysample(s,i) = 1-mean(predsame_bin(st1~=st2));
    end
    
    pbf_bin_tmp = squeeze(preds_by_file_binmean(:,:,s,:));
    pbf_bin_tmp = sum(pbf_bin_tmp,3); %sum over its, now files x seiztypes
    [~,pbf_bin_classes] = max(pbf_bin_tmp,[],2);
    for j = 1:n_seiz_types
        corr_pos_tmp = sum(pbf_bin_classes==file_seiztypes & file_seiztypes==j);
        all_pos_tmp  = sum(pbf_bin_classes==j);
        rel_sam_tmp  = sum(file_seiztypes==j);
        prec_tmp     = corr_pos_tmp / all_pos_tmp;
        reca_tmp     = corr_pos_tmp / rel_sam_tmp;
        f1(s,j)      = 2 * ((prec_tmp * reca_tmp) / (prec_tmp + reca_tmp)); %#ok<AGROW,NASGU>
        
    end
end

keyboard;

